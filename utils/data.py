import re
import textblob
import pandas as pd

TWEETS_FILE_DELIMITER = ",,,#,,,~,,,#,,,"


def get_pandas_df_from__001_file():
    return pd.read_csv("./data/_001_tweets_for___#nrc_caa_protest.csv",  sep=TWEETS_FILE_DELIMITER, engine='python')


def clean_tweet(tweet_txt, remove_hashtag=True, remove_mentions=True, remove_retweet_RT=True, remove_urls=True, remove_special_characters=True):
    if type(tweet_txt) == bytes:
        tweet_txt = tweet_txt.decode('ascii', 'ignore')
    tweet_txt = str(tweet_txt)
    if remove_retweet_RT:
        tweet_txt = re.sub(r'^RT\s*@[\w\s]*:\s*', ' ', tweet_txt)  # remove RT
    if remove_hashtag:
        tweet_txt = re.sub(r'#[\w\d]*', ' ', tweet_txt)  # remove hash tags
    if remove_mentions:
        tweet_txt = re.sub(r'@[\w\d]*', ' ', tweet_txt)  # remove mentions
    if remove_urls: # remove urls
        tweet_txt = re.sub(r'http[\w\d\:\./]*', ' ', tweet_txt)
        tweet_txt = re.sub(r'www[\w\d\:\./]*', ' ', tweet_txt)
    if remove_special_characters:
        tweet_txt = re.sub(r'[^a-zA-Z\d\-_]+', ' ', tweet_txt)

    tweet_txt = re.sub(r'\r\n+', ' ', tweet_txt)  # remove line change
    tweet_txt = re.sub(r'\n+', ' ', tweet_txt)  # remove line change
    tweet_txt = re.sub(r'&amp;', ' ', tweet_txt)
    tweet_txt = re.sub(r'&amp', ' ', tweet_txt)
    tweet_txt = re.sub(r'&amp', ' ', tweet_txt)
    tweet_txt = re.sub(r' +', ' ', tweet_txt)  # replace consecutive spaces with space
    return tweet_txt


def get_text_and_subjectivity_tuple(text):
    doc = textblob.TextBlob(text)
    return text, doc.sentiment.subjectivity

