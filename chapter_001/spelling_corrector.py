import re
import string

from nltk.corpus import words

all_english_words = words.words()

# This is just a demo spell corrector, it is not ready for production/industrialisation
# Also there is no machine learning / deep learning in this implementation


class SpellCorrector:

    def get_words_from_text_by_splitting_with_space(text):
        return re.split(r' ', text.lower())

    def get_corrected_word(self, word):
        if word in all_english_words:
            return word
        return max(self.get_set_of_unique_candidates(word))

    def get_set_of_unique_candidates(self, word):
        return (self.set_of_valid_words_from_given_iterator([word]) or self.set_of_valid_words_from_given_iterator(
            self.change_by_one_character(word)) or self.set_of_valid_words_from_given_iterator(self.change_by_two_character(word)) or [word])

    def set_of_valid_words_from_given_iterator(self, words):
        return set(w for w in words if w in all_english_words)

    def change_by_one_character(self, word):
        letters = string.ascii_lowercase
        splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]
        deletes = [left_split + right_split[1:] for left_split, right_split in splits if right_split]
        swaps_adjacent = [left_split + right_split[1] + right_split[0] + right_split[2:] for left_split, right_split in splits if len(right_split) > 1]
        replaces = [left_split + character + right_split[1:] for left_split, right_split in splits if right_split for character in letters]
        inserts = [left_split + character + right_split for left_split, right_split in splits for character in letters]
        return set(deletes + swaps_adjacent + replaces + inserts)

    def change_by_two_character(self, word):
        return (e2 for e1 in self.change_by_one_character(word) for e2 in self.change_by_one_character(e1))


if __name__ == "__main__":
    spell_corrector = SpellCorrector()
    print(spell_corrector.get_corrected_word("I"))
    print(spell_corrector.get_corrected_word("lovve"))
    print(spell_corrector.get_corrected_word("mahcine"))
    print(spell_corrector.get_corrected_word("learnig"))
